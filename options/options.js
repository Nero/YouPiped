// Restore options
document.addEventListener("DOMContentLoaded", () => {
  browser.storage.local.get({
    instance: "piped.kavin.rocks",
    proxy: "pipedproxy.kavin.rocks"
  }).then((storage) => {
    document.querySelector("#instance").value = storage.instance;
    document.querySelector("#proxy").value = storage.proxy;
  });
});



// Save options
const saveBtn = document.getElementById("saveBtn");

saveBtn.addEventListener("click", (e) => {
  e.preventDefault();
  const instance = document.querySelector("#instance").value.trim().replace(/^https?:\/\/*|\/$/g, '');
  const proxy = document.querySelector("#proxy").value.trim().replace(/^https?:\/\/*|\/$/g, '');

  if (!instance || !proxy) {
    return alert("Please fill out the fields!");
  }

  browser.storage.local.set({
    instance,
    proxy,
  }).then(() => {
    saveBtn.disabled = true;
    saveBtn.value = "Saved!";
    setTimeout(() => {
      saveBtn.disabled = false;
      saveBtn.value = "Save";
    }, 3000)
  });
});
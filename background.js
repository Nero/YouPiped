let w2gRegex = /w2g\.tv/;
let tabWhitelist = [];

// Einstellungen ziehen, ContentScript registrieren
browser.storage.local.get({
  activated: true,
  instance: "piped.kavin.rocks",
})
  .then((storage) => {
    if (storage.activated) {
      activateBlocker();
    } else {
      deactivateBlocker();
    }

    registerCS(storage.instance);
  });

// ContentScript für aktuelle Instanz registrieren
async function registerCS(url) {
  return await browser.contentScripts.register({
    js: [{ file: "content.js" }],
    matches: ["*://" + url + "/*"],
  });
}

// pageAction anzeigen
browser.runtime.onMessage.addListener((data, sender) => {
  if (data === "showYoutubePageAction") {
    browser.pageAction.setIcon({
      tabId: sender.tab.id,
      path: "icons/youtube.png",
    });
    browser.pageAction.setTitle({
      tabId: sender.tab.id,
      title: "Open on YouTube",
    });
    browser.pageAction.show(sender.tab.id);

  } else if (data === "showPipedPageAction") {
    browser.pageAction.setIcon({
      tabId: sender.tab.id,
      path: "icons/piped.png",
    });
    browser.pageAction.setTitle({
      tabId: sender.tab.id,
      title: "Open on Piped",
    });
    browser.pageAction.show(sender.tab.id);
  }
});

// Storage onChange listener
browser.storage.onChanged.addListener((changes) => {
  if ("instance" in changes) {
    registerCS(changes.instance.newValue);
  }
  if ("enabledBlock" in changes) {
    enabledBlock = changes.enabledBlock.newValue;
  }
});

// Wenn Icon geklickt wird Blocker de-/aktivieren
browser.browserAction.onClicked.addListener(() => {
  browser.storage.local.get("activated").then((item) => {
    if (item.activated) {
      deactivateBlocker();
    } else {
      activateBlocker();
    }
  });
});

function activateBlocker() {
  browser.browserAction.setIcon({ path: "icons/piped.png" });
  browser.storage.local.set({ activated: true });
}

function deactivateBlocker() {
  browser.browserAction.setIcon({ path: "icons/youtube.png" });
  browser.storage.local.set({ activated: false });
}

// Redirect
browser.webRequest.onBeforeRequest.addListener(
  async (details) => {
    if (!tabWhitelist.includes(details.tabId)) {
      let activated;
      let instance;
      let proxy;

      await browser.storage.local.get({
        activated: true,
        instance: "piped.kavin.rocks",
        proxy: "pipedproxy.kavin.rocks",
      })
        .then((storage) => {
          activated = storage.activated;
          instance = storage.instance;
          proxy = storage.proxy;
        });

      if (activated) {
        const youtubeRegex = /youtube.com(\/?.*)/;
        const youtubeShortRegex = /youtu.be\/.+/;

        if (details.url.startsWith("https://img.youtube.com/vi")) {
          return {
            redirectUrl: `https://${proxy}/vi/${details.url.split('/vi/')[1].split('/')[0]}/hqdefault.jpg?host=i.ytimg.com`
          };
        } else if (youtubeRegex.test(details.url)) {
          return {
            redirectUrl: "https://" + instance + youtubeRegex.exec(details.url)[1],
          };
        } else if (youtubeShortRegex.test(details.url)) {
          const youtubeShortCaptureRegex = /youtu.be\/(.+)/;
          return {
            redirectUrl: "https://" + instance + "/watch?v=" + youtubeShortCaptureRegex.exec(details.url)[1],
          };
        } else if (
          details.url.startsWith("https://www.youtube-nocookie.com/embed/")
        ) {
          return {
            redirectUrl: "https://" + instance + "/embed/" + details.url.split("/embed/")[1]
          };
        }
      }
    }
  },

  {
    urls: [
      "*://*.youtube.com/*",
      "*://*.youtu.be/*",
      "*://*.youtube-nocookie.com/*",
    ],
  },

  ["blocking"]
);

// URL Icon Click --> Open on YouTube/Piped
browser.pageAction.onClicked.addListener((tab) => {
  browser.storage.local.get({
    activated: true,
    instance: "piped.kavin.rocks"
  })
    .then(async (storage) => {
      if (tab.url.startsWith("https://www.youtube.com")) {
        browser.tabs.create({
          url: "https://" + storage.instance + "/" + tab.url.split("/")[3],
        });
      } else {
        let newTab = await browser.tabs.create({
          url: "https://www.youtube.com/" + tab.url.split("/")[3],
        });
        if (!tabWhitelist.includes(newTab.id)) {
          tabWhitelist.push(newTab.id);
        }
      }
    });
});

// Close tab -> Remove from whitelist
browser.tabs.onRemoved.addListener((tabId) => {
  if (tabWhitelist.includes(tabId)) {
    tabWhitelist.splice(tabWhitelist.indexOf(tabId), 1);
  }
});
